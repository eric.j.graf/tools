#!/usr/bin/env bash

#set -x

SOURCE=${BASH_SOURCE[0]}

while [ -h "$SOURCE" ]; do 
  DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
  SOURCE=$(readlink "$SOURCE")
  [[ $SOURCE != /* ]] && SOURCE=$DIR/$SOURCE 
done

DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )


if [[ "$(docker images -q openstack-cli 2> /dev/null)" == "" ]]; then
    docker build ${DIR} -t openstack-cli #> /dev/null
fi


docker run --rm -it \
    --network=host \
    -e OS_AUTH_URL=$OS_AUTH_URL \
    -e OS_PROJECT_ID=$OS_PROJECT_ID\
    -e OS_PROJECT_NAME=$OS_PROJECT_NAME \
    -e OS_USER_DOMAIN_NAME=$OS_USER_DOMAIN_NAME \
    -e OS_PROJECT_DOMAIN_ID=$OS_PROJECT_DOMAIN_ID \
    -e OS_USERNAME=$OS_USERNAME \
    -e OS_PASSWORD=$OS_PASSWORD \
    -e OS_REGION_NAME=$OS_REGION_NAME \
    -e OS_INTERFACE=$OS_INTERFACE \
    -e OS_IDENTITY_API_VERSION=$OS_IDENTITY_API_VERSION \
    -v ~/.config/openstack/:/root/.config/openstack \
    openstack-cli $@
