#!/usr/bin/bash

export KUBERNETES_SERIES=`echo "${KUBERNETES_VERSION%.*}"`

cat  /home/imagebuilder/packer/config/kubernetes.json | \
    jq ".kubernetes_semver = \"v${KUBERNETES_VERSION}\""  > \
    tmp && \
    mv tmp /home/imagebuilder/packer/config/kubernetes.json
    
cat /home/imagebuilder/packer/config/kubernetes.json | \
    jq ".kubernetes_deb_version = \"${KUBERNETES_VERSION}-00\""  > \
    tmp && \
    mv tmp /home/imagebuilder/packer/config/kubernetes.json
    
cat /home/imagebuilder/packer/config/kubernetes.json | \
    jq ".kubernetes_series = \"v${KUBERNETES_SERIES}\""  > \
    tmp && \
    mv tmp /home/imagebuilder/packer/config/kubernetes.json

cat /home/imagebuilder/packer/qemu/packer.json | \
    jq ".variables.accelerator = \"${ACCELERATOR}\"" > \
    tmp && \
    mv tmp /home/imagebuilder/packer/qemu/packer.json

cat /home/imagebuilder/packer/raw/packer.json | \
    jq ".variables.accelerator = \"${ACCELERATOR}\"" > \
    tmp && \
    mv tmp /home/imagebuilder/packer/raw/packer.json

cat /home/imagebuilder/packer/qemu/packer.json | \
    jq ".variables.cpus = \"${CPUS}\"" > \
    tmp && \
    mv tmp /home/imagebuilder/packer/qemu/packer.json

cat /home/imagebuilder/packer/qemu/packer.json | \
    jq ".variables.memory = \"${MEMORY}\"" > \
    tmp && \
    mv tmp /home/imagebuilder/packer/qemu/packer.json

cat /home/imagebuilder/packer/qemu/packer.json
cat /home/imagebuilder/packer/raw/packer.json

exec "$@"
