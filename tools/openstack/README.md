## Build docker image

`docker build -t openstackcli .`


## Pip packages

https://pypi.org/project/python-openstackclient/#files

https://pypi.org/user/openstackci/

## Run image

```
export OS_AUTH_URL=<url-to-openstack-identity>
export OS_IDENTITY_API_VERSION=3
export OS_PROJECT_NAME=<project-name>
export OS_PROJECT_DOMAIN_NAME=<project-domain-name>
export OS_USERNAME=<username>
export OS_USER_DOMAIN_NAME=<user-domain-name>
export OS_PASSWORD=<password>  # (optional)
```


