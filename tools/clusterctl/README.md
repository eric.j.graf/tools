# cmd ref


https://cluster-api.sigs.k8s.io/clusterctl/commands/commands.html



# Run command

```
docker run --rm \
     -v ~/.kube/config:/kube/config \
     --env KUBECONFIG=/kube/config \
     registry.gitlab.com/eric.j.graf/tools/clusterctl \
     clusterctl <cmd>

```


## 

```
alias CLUSTERCTL="docker run --rm \
     -v ~/.kube/config:/kube/config \
     --env KUBECONFIG=/kube/config \
     registry.gitlab.com/eric.j.graf/tools/clusterctl \
     clusterctl "

${CLUSTERCTL} upgrade plan	

${CLUSTERCTL} upgrade apply

${CLUSTERCTL} upgrade apply --contract v1beta1

${CLUSTERCTL} backup

${CLUSTERCTL} restore
```