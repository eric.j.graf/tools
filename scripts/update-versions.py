#!/usr/bin/env python3

from os import walk
import yaml
import re
import os

def main():
    mypath = "../"

    for (dirpath, dirnames, filenames) in walk(mypath):
        for fn in filenames:
            #print("{}/{}".format(dirpath,fn))
            if fn == "latest-version.sh":
                print ("{}/{}".format(dirpath,fn))
                os.system("sh {}/{} > {}/version".format(dirpath,fn,dirpath))

if __name__ == "__main__":
    main()
