# cmd ref

# Run command

```
docker run --rm \
     -v $(pwd)/output:/output \
     --env KUBERNETES_VERSION=1.24.11 \
     registry.gitlab.com/eric.j.graf/tools/image-builder \
     make  build-qemu-ubuntu-2004

```


# Build and Upload image

## ENV variables

```

k8s_release=1.24.11
FILE_NAME=ubuntu-2004-kube-v${k8s_release}
OUTPUT_FILE=/home/imagebuilder/output/${FILE_NAME}/${FILE_NAME}


```

## Build
```
cd scripts/docker-image-builder

mkdir output
chmod 777 output

sudo chmod 666 /dev/kvm


docker run --rm \
     --env KUBERNETES_VERSION=${k8s_release} \
     --env CPUS=8 \
     --env MEMORY=8192 \
     --env ACCELERATOR=kvm \
     --network="host" \
     --device=/dev/kvm \
     -v `pwd`/output:/home/imagebuilder/output \
     registry.gitlab.com/eric.j.graf/tools/image-builder \
     make  build-qemu-ubuntu-2004

```

## upload image

```
source openrc.sh

alias openstack_cli="docker run --rm -it \
    --network=host \
    -e OS_AUTH_URL=$OS_AUTH_URL \
    -e OS_PROJECT_ID=$OS_PROJECT_ID\
    -e OS_PROJECT_NAME=$OS_PROJECT_NAME \
    -e OS_USER_DOMAIN_NAME=$OS_USER_DOMAIN_NAME \
    -e OS_PROJECT_DOMAIN_ID=$OS_PROJECT_DOMAIN_ID \
    -e OS_USERNAME=$OS_USERNAME \
    -e OS_PASSWORD=$OS_PASSWORD \
    -e OS_REGION_NAME=$OS_REGION_NAME \
    -e OS_INTERFACE=$OS_INTERFACE \
    -e OS_IDENTITY_API_VERSION=$OS_IDENTITY_API_VERSION \
    -v ~/.config/openstack/:/root/.config/openstack \
    -v `pwd`/output:/output \
    registry.gitlab.com/eric.j.graf/tools/openstack:latest "

openstack_cli openstack image create \
 --disk-format qcow2 --container-format bare \
 --file /output/$FILE_NAME/$FILE_NAME \
 $FILE_NAME --progress

```